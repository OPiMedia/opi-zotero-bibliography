#!/bin/sh

sed -E -i 's/@misc\{porte_dans_2021/@audio{porte_dans_2021/g' 'My Library.bib'

chmod -R og-rwx 'My Library.bib' 'Zotero/My Library.rdf'
