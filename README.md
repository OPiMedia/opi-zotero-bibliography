# OPi Zotero bibliography

Bakup of my Zotero bibliography <https://www.zotero.org/opimedia>

  - [`bib/`](https://bitbucket.org/OPiMedia/opi-zotero-bibliography/src/master/bib/): **BibLaTeX and Zotero data**
  - [`biblio_src/`](https://bitbucket.org/OPiMedia/opi-zotero-bibliography/src/master/biblio_src/): LaTeX source of the PDF document
  - [**`opi-zotero-bibliography.pdf`**](https://bitbucket.org/OPiMedia/opi-zotero-bibliography/raw/master/opi-zotero-bibliography.pdf): the list by category in a PDF document



![OPi Zotero](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2019/Nov/13/1083871536-2-opi-zotero-bibliography-logo_avatar.png)
